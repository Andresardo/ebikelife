//
//  Weather.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation
import CoreLocation

struct Weather {
    let summary:String
    let icon:String
    let temperature:Double
    let time:Double
    
    enum SerializationError:Error {
        case missing(String)
        case invalid(String, Any)
    }
    
    
    init(json:[String:Any]) throws {
        guard let summary = json["summary"] as? String else {throw SerializationError.missing("summary is missing")}
        guard let icon = json["icon"] as? String else {throw SerializationError.missing("icon is missing")}
        guard let temperature = json["temperature"] as? Double else {throw SerializationError.missing("temp is missing")}
        guard let time = json["time"] as? Double else {throw SerializationError.missing("time is missing")}
        
        self.summary = summary
        self.icon = icon
        self.temperature = temperature
        self.time = time
    }
    
    // parte inicial do pedido da API, contém a chave da API
    static let basePath = "https://api.darksky.net/forecast/36c7460e5190e689f29f59826074e01f/"
    
    static func forecast (withLocation location:CLLocationCoordinate2D, completion: @escaping ([Weather]?) -> ()) {
        // indicação da localização atual para o pedido
        let url = basePath + "\(location.latitude),\(location.longitude)?units=si"
        let request = URLRequest(url: URL(string: url)!)
        let task = URLSession.shared.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            var forecastArray:[Weather] = []
            
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
                        if let dailyForecasts = json["hourly"] as? [String:Any] {
                            if let dailyData = dailyForecasts["data"] as? [[String:Any]] {
                                for dataPoint in dailyData {
                                    if let weatherObject = try? Weather(json: dataPoint) {
                                        forecastArray.append(weatherObject)
                                    }
                                }
                            }
                        }
                    }
                }catch {
                    print(error.localizedDescription)
                }
                completion(forecastArray)
            }
        }
        task.resume()
    }
}
