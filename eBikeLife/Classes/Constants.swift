//
//  Constants.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation

// constantes para a gravação dos dados em ficheiros
struct Constants {
    static let ROUTE = "route"
    static let INIT_TIME = "initTime"
    static let END_TIME = "endTime"
    static let DISTANCE = "distance"
    static let DURATION = "duration"
    static let INIT_PLACE = "initPlace"
    static let END_PLACE = "endPlace"
    static let VELOCITY = "velocity"
    static let PLACE = "place"
    static let FINISHED = "finished"
    static let ITIME = "iTime"
    static let ETIME = "eTime"
    static let DAY_DIST = "dayDist"
    static let WEEK_DIST = "weekDist"
    static let MONTH_DIST = "monthDist"
    static let DAY_TIME = "dayTime"
    static let WEEK_TIME = "weekTime"
    static let MONTH_TIME = "monthTIME"
    static let DAY_SAVE = "daySave"
    static let WEEK_SAVE = "weekSave"
    static let MONTH_SAVE = "monthSave"
    
    // diretoria para gravar ficheiros
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let ArchiveRoute = Constants.DocumentsDirectory.appendingPathComponent("routes.archive").path
    static let ArchiveCharges = Constants.DocumentsDirectory.appendingPathComponent("charges.archive").path
    static let ArchiveObjectives = Constants.DocumentsDirectory.appendingPathComponent("objectives.archive").path
}
