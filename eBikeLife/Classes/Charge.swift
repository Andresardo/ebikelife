//
//  Charge.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation
import CoreLocation

// Classe para os carregamentos com os campos necessários
class Charge: NSObject, NSCoding {
    var place : String
    var initTime : String = " "
    var endTime : String = " "
    var duration : Double
    var finished : Bool = false
    var iTime : Double
    
    // função encode permite formatar os dados de forma a serem guardados em ficheiros
    func encode(with aCoder: NSCoder) {
        aCoder.encode(place, forKey: Constants.PLACE)
        aCoder.encode(initTime, forKey: Constants.INIT_TIME)
        aCoder.encode(endTime, forKey: Constants.END_TIME)
        aCoder.encode(duration, forKey: Constants.DURATION)
        aCoder.encode(finished, forKey: Constants.FINISHED)
        aCoder.encode(iTime, forKey: Constants.ITIME)
    }
    
    init(place:String, initTime: String, endTime: String, duration: Double, finished: Bool, iTime: Double){
        self.place = place
        self.initTime = initTime
        self.endTime = endTime
        self.duration = duration
        self.finished = finished
        self.iTime = iTime
    }
    
    // inicialisar os dados quando são lidos de um ficheiro
    required init?(coder aDecoder: NSCoder) {
        self.place = aDecoder.decodeObject(forKey: Constants.PLACE) as! String
        self.initTime = aDecoder.decodeObject(forKey: Constants.INIT_TIME) as! String
        self.endTime = aDecoder.decodeObject(forKey: Constants.END_TIME) as! String
        self.duration = aDecoder.decodeDouble(forKey: Constants.DURATION)
        self.finished = aDecoder.decodeBool(forKey: Constants.FINISHED)
        self.iTime = aDecoder.decodeDouble(forKey: Constants.ITIME)
    }
}
