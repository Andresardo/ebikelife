//
//
//  Objetivos
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation

class Objectives: NSObject, NSCoding {
    var dayDist : Double
    var dayTime : Double
    var daySave : Double
    var weekDist : Double
    var weekTime : Double
    var weekSave : Double
    var monthDist : Double
    var monthTime : Double
    var monthSave : Double
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(dayDist, forKey: Constants.DAY_DIST)
        aCoder.encode(dayTime, forKey: Constants.DAY_TIME)
        aCoder.encode(daySave, forKey: Constants.DAY_SAVE)
        aCoder.encode(weekDist, forKey: Constants.WEEK_DIST)
        aCoder.encode(weekTime, forKey: Constants.WEEK_TIME)
        aCoder.encode(weekSave, forKey: Constants.WEEK_SAVE)
        aCoder.encode(monthDist, forKey: Constants.MONTH_DIST)
        aCoder.encode(monthTime, forKey: Constants.MONTH_TIME)
        aCoder.encode(monthSave, forKey: Constants.MONTH_SAVE)
    }
    
    init(dayDist: Double, dayTime: Double, daySave: Double, weekDist: Double, weekTime: Double, weekSave: Double, monthDist: Double, monthTime: Double, monthSave: Double){
        self.dayDist = dayDist
        self.dayTime = dayTime
        self.daySave = daySave
        self.weekDist = weekDist
        self.weekTime = weekTime
        self.weekSave = weekSave
        self.monthDist = monthDist
        self.monthTime = monthTime
        self.monthSave = monthSave
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.dayDist = aDecoder.decodeDouble(forKey: Constants.DAY_DIST)
        self.dayTime = aDecoder.decodeDouble(forKey: Constants.DAY_TIME)
        self.daySave = aDecoder.decodeDouble(forKey: Constants.DAY_SAVE)
        self.weekDist = aDecoder.decodeDouble(forKey: Constants.WEEK_DIST)
        self.weekTime = aDecoder.decodeDouble(forKey: Constants.WEEK_TIME)
        self.weekSave = aDecoder.decodeDouble(forKey: Constants.WEEK_SAVE)
        self.monthDist = aDecoder.decodeDouble(forKey: Constants.MONTH_DIST)
        self.monthTime = aDecoder.decodeDouble(forKey: Constants.MONTH_TIME)
        self.monthSave = aDecoder.decodeDouble(forKey: Constants.MONTH_SAVE)
    }
}
