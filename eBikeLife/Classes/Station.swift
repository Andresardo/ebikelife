//
//  Station.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation
import CoreLocation

class Station {
    var location : CLLocation
    var place : String

    init(location:CLLocation, place: String){
        self.location = location
        self.place = place
    }
}
