//
//  MeteorologyViewCellCollectionViewCell.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit

//informação para as células da collection view
class MeteorologyViewCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weekDayLabel: UILabel!
}
