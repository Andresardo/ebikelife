//
//  Percurso.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation
import CoreLocation

class Route: NSObject, NSCoding {
    var route = [CLLocation]()
    var initTime : String = " "
    var endTime : String = " "
    var distance : Double
    var duration : Double
    var initPlace : String
    var endPlace : String
    var velocity : Double
    var eTime : Double
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(route, forKey: Constants.ROUTE)
        aCoder.encode(initTime, forKey: Constants.INIT_TIME)
        aCoder.encode(endTime, forKey: Constants.END_TIME)
        aCoder.encode(duration, forKey: Constants.DURATION)
        aCoder.encode(distance, forKey: Constants.DISTANCE)
        aCoder.encode(initPlace, forKey: Constants.INIT_PLACE)
        aCoder.encode(endPlace, forKey: Constants.END_PLACE)
        aCoder.encode(velocity, forKey: Constants.VELOCITY)
        aCoder.encode(eTime, forKey: Constants.ETIME)
    }
    
    init(route:[CLLocation], initTime: String, endTime: String, distance: Double, duration: Double, initPlace: String, endPlace: String, velocity: Double, eTime: Double){
        self.route = route
        self.initTime = initTime
        self.endTime = endTime
        self.distance = distance
        self.duration = duration
        self.initPlace = initPlace
        self.endPlace = endPlace
        self.velocity = velocity
        self.eTime = eTime
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.route = aDecoder.decodeObject(forKey: Constants.ROUTE) as! [CLLocation]
        self.initTime = aDecoder.decodeObject(forKey: Constants.INIT_TIME) as! String
        self.endTime = aDecoder.decodeObject(forKey: Constants.END_TIME) as! String
        self.duration = aDecoder.decodeDouble(forKey: Constants.DURATION)
        self.distance = aDecoder.decodeDouble(forKey: Constants.DISTANCE)
        self.initPlace = aDecoder.decodeObject(forKey: Constants.INIT_PLACE) as! String
        self.endPlace = aDecoder.decodeObject(forKey: Constants.END_PLACE) as! String
        self.velocity = aDecoder.decodeDouble(forKey: Constants.VELOCITY)
        self.eTime = aDecoder.decodeDouble(forKey: Constants.ETIME)
    }
}
