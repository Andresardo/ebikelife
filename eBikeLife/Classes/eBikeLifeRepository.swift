//
//  eBikeLifeRepository.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import Foundation

//Repositório para partilhar os dados entre os vários controladores
class eBikeLifeRepository{
    
    static let repository = eBikeLifeRepository()
    var routes = [Route]()
    var charges = [Charge]()
    var objectives = [Objectives]()
    var stations = [Station]()
    
    private init() {}
    
    // guardar para ficheiro
    func saveRoutes(){
        print("saving to: \(Constants.ArchiveRoute)")
        NSKeyedArchiver.archiveRootObject(routes, toFile: Constants.ArchiveRoute)
    }
    // ler a partir do ficheiro
    func getRoutes(){
        if let p = NSKeyedUnarchiver.unarchiveObject(withFile: Constants.ArchiveRoute) as? [Route]{
            routes = p
            print("got \(routes.count) routes")
        }
    }
    
    func saveCharges(){
        print("saving to: \(Constants.ArchiveCharges)")
        NSKeyedArchiver.archiveRootObject(charges, toFile: Constants.ArchiveCharges)
    }
    
    func getCharges(){
        if let p = NSKeyedUnarchiver.unarchiveObject(withFile: Constants.ArchiveCharges) as? [Charge]{
            charges = p
            print("got \(charges.count) charges")
        }
    }
    
    func saveObjectives(){
        print("saving to: \(Constants.ArchiveObjectives)")
        NSKeyedArchiver.archiveRootObject(objectives, toFile: Constants.ArchiveObjectives)
    }
    
    func getObjectives(){
        if let p = NSKeyedUnarchiver.unarchiveObject(withFile: Constants.ArchiveObjectives) as? [Objectives]{
            objectives = p
            print("got \(objectives.count) objectives")
        }
    }
}
