//
//  ObjectivesTableViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit

class ObjectivesTableViewController: UITableViewController {
    
    let sections: [String] = ["Diário","Semanal","Mensal"]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Ajustar a largura da view do side menu
        let newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*4/5, height: UIScreen.main.bounds.height)
        self.view.frame = newFrame
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    // Escreve as informações dos objetivos na tabela
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell5", for: indexPath)
        switch indexPath.section {
        case 0: // Today
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Distância"
                cell.detailTextLabel?.text = String(format:"%.0fkm",eBikeLifeRepository.repository.objectives[0].dayDist/1000)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            case 1:
                cell.textLabel?.text = "Tempo"
                cell.detailTextLabel?.text = String(format:"%.0fh",eBikeLifeRepository.repository.objectives[0].dayTime/3600)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            case 2:
                cell.textLabel?.text = "Poupançaa"
                cell.detailTextLabel?.text = String(format:"%.0f€",eBikeLifeRepository.repository.objectives[0].daySave)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            default:
                print("nothing")
            }
            
        case 1:
            switch indexPath.row {
            case 0: // This Week
                cell.textLabel?.text = "Distância"
                cell.detailTextLabel?.text = String(format:"%.0fkm",eBikeLifeRepository.repository.objectives[0].weekDist/1000)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            case 1:
                cell.textLabel?.text = "Tempo"
                cell.detailTextLabel?.text = String(format:"%.0fh",eBikeLifeRepository.repository.objectives[0].weekTime/3600)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            case 2:
                cell.textLabel?.text = "Poupançaa"
                cell.detailTextLabel?.text = String(format:"%.0f€",eBikeLifeRepository.repository.objectives[0].weekSave)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            default:
                print("nothing")
            }
        case 2: //This Month
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Distância"
                cell.detailTextLabel?.text = String(format:"%.0fkm",eBikeLifeRepository.repository.objectives[0].monthDist/1000)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            case 1:
                cell.textLabel?.text = "Tempo"
                cell.detailTextLabel?.text = String(format:"%.0fh",eBikeLifeRepository.repository.objectives[0].monthTime/3600)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            case 2:
                cell.textLabel?.text = "Poupançaa"
                cell.detailTextLabel?.text = String(format:"%.0f€",eBikeLifeRepository.repository.objectives[0].monthSave)
                cell.detailTextLabel?.textColor = UIColor.lightGray
            default:
                print("nothing")
            }
        default:
            print("nothing")
        }
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    // Quando se seleciona uma célula, mostra-se um alert com campo editável
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: // today
            switch indexPath.row {
            case 0:
                inputAlert(message: "Alterar distância (km)", title: "Objetivo diário", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].dayDist/1000), indexPath: indexPath)
            case 1:
                inputAlert(message: "Alterar tempo (horas)", title: "Objetivo diário", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].dayTime/3600), indexPath: indexPath)
            case 2:
                inputAlert(message: "Alterar poupança (€)", title: "Objetivo diário", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].daySave), indexPath: indexPath)
            default:
                print("nothing")
            }
            
        case 1: // this week
            switch indexPath.row {
            case 0:
                inputAlert(message: "Alterar distância (km)", title: "Objetivo semanal", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].weekDist/1000), indexPath: indexPath)
            case 1:
                inputAlert(message: "Alterar tempo (horas)", title: "Objetivo semanal", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].weekTime/3600), indexPath: indexPath)
            case 2:
                inputAlert(message: "Alterar poupança (€)", title: "Objetivo semanal", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].weekSave), indexPath: indexPath)
            default:
                print("nothing")
            }
        case 2: // this month
            switch indexPath.row {
            case 0:
                inputAlert(message: "Alterar distância (km)", title: "Objetivo mensal", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].monthDist/1000), indexPath: indexPath)
            case 1:
                inputAlert(message: "Alterar tempo (horas)", title: "Objetivo mensal", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].monthTime/3600), indexPath: indexPath)
            case 2:
                inputAlert(message: "Alterar poupança (€)", title: "Objetivo mensal", vc: self, text: String(format:"%.0f",eBikeLifeRepository.repository.objectives[0].monthSave), indexPath: indexPath)
            default:
                print("nothing")
            }
        default:
            print("nothing")
        }
    }
    
    // Alert com campo editável para atualizar um objetivo
    func inputAlert(message: String, title: String, vc: UIViewController, text: String, indexPath: IndexPath){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.text = text
        }
        
        alert.addAction(UIAlertAction(title: "Guardar", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            
            switch indexPath.section {
            case 0: // today
                switch indexPath.row {
                case 0:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].dayDist = value!*1000
                            }
                        }
                    }
                case 1:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].dayTime = value!*3600
                            }
                        }
                    }
                case 2:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].daySave = value!
                            }
                        }
                    }
                default:
                    print("nothing")
                }
                
            case 1: // this week
                switch indexPath.row {
                case 0:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].weekDist = value!*1000
                            }
                        }
                    }
                case 1:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].weekTime = value!*3600
                            }
                        }
                    }
                case 2:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].weekSave = value!
                            }
                        }
                    }
                default:
                    print("nothing")
                }
            case 2: // this month
                switch indexPath.row {
                case 0:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].monthDist = value!*1000
                            }
                        }
                    }
                case 1:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].monthTime = value!*3600
                            }
                        }
                    }
                case 2:
                    if let text = textField?.text {
                        let value = Double(text)
                        if value != nil{
                            if value! > 0.0 {
                                eBikeLifeRepository.repository.objectives[0].monthSave = value!
                            }
                        }
                    }
                default:
                    print("nothing")
                }
            default:
                print("nothing")
            }
            eBikeLifeRepository.repository.saveObjectives()
            self.tableView.reloadData()
        }))
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
