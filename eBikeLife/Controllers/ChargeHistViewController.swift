//
//  ChargeHistViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit

class ChargeHistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    var localList = [Charge]()
    
    @IBOutlet weak var selector: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func segmentedControlActionChange(_ sender: UISegmentedControl) {
        switch selector.selectedSegmentIndex {
        case 0:
            localList.sort(by: {$0.iTime > $1.iTime})
            break
        case 1:
            localList.sort(by: {$0.duration > $1.duration})
            break
        default:
            break
        }
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        localList = eBikeLifeRepository.repository.charges
        selector.selectedSegmentIndex = 0
        segmentedControlActionChange(selector)
        
        // Ajustar a largura da view do side menu
        let newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*4/5, height: UIScreen.main.bounds.height)
        self.view.frame = newFrame
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell2 = tableView.dequeueReusableCell(withIdentifier: "myCell2", for: indexPath)
        myCell2.textLabel!.text = ("Local: \(localList[indexPath.row].place) \nInicio: \(localList[indexPath.row].initTime) \nFim: \(localList[indexPath.row].endTime) \nDuração: \(convertTime(duration: localList[indexPath.row].duration))")
        myCell2.textLabel?.adjustsFontSizeToFitWidth = true
        return myCell2
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Se não for fazer o segue, volta para trás
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    // Permite transformar um valor em segundos para horas:minutos:segundos
    func convertTime(duration: Double) -> String{
        if duration < 60 {
            let seconds = round(duration)
            return String(format: "%.0fs", seconds)
        } else if (duration > 60 && duration < 3600) {
            let minutes = floor(duration/60)
            let seconds = round (duration - minutes*60)
            return String(format: "%.0fm %.0fs",minutes, seconds)
        } else if (duration > 3600 && duration < 86400) {
            let hours = floor(duration/3600)
            let minutes = floor((duration - hours*3600)/60)
            let seconds = round(duration - hours*3600 - minutes*60)
            return String(format: "%.0fh %.0fm %.0fs", hours, minutes, seconds)
        } else {
            let days = floor(duration/86400)
            let hours = floor((duration - days*86400)/3600)
            let minutes = floor((duration - days*86400 - hours*3600)/60)
            return String(format: "%.0fd %.0fh %.0fm",days , hours, minutes)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Remover carregamento e atualizar tabela
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            for index in 0...eBikeLifeRepository.repository.charges.count-1{
                if eBikeLifeRepository.repository.charges[index] == localList[indexPath.row]{
                    eBikeLifeRepository.repository.charges.remove(at: index)
                    break
                }
            }
            localList.remove(at: indexPath.row)
            eBikeLifeRepository.repository.saveCharges()
            tableView.reloadData()
        }
    }
}
