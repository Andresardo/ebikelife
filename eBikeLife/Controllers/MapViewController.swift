//
//  MapViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol HandleMapSearch {
    func dropPinZoomIn(placemark:MKPlacemark)
}

class MapViewController : UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, HandleMapSearch {
    
    // Atualiza a variável local quando a variável do AppDelegate é alterada
    var locationManager: CLLocationManager {
        return AppDelegate.locationManager
    }
    
    var tracking: Bool {
        get{
            return AppDelegate.tracking
        }
        set (newTracking){
            AppDelegate.tracking = newTracking
        }
    }
    
    var route: [CLLocation] {
        get{
            return AppDelegate.route
        }
        set (newRoute){
            AppDelegate.route = newRoute
        }
    }
    
    var iTime: Double {
        get{
            return AppDelegate.iTime
        }
        set (newiTime){
            AppDelegate.iTime = newiTime
        }
    }
    
    var currentLocation:CLLocation{
        get{
            return AppDelegate.currentLocation
        }
        set (newL){
            AppDelegate.currentLocation = newL
        }
    }
    
    var station : Station?
    var selectedPin:MKPlacemark? = nil
    var resultSearchController:UISearchController? = nil
    var blueRoute = true
    let dateform = DateFormatter()
    var currentPlacemark: CLPlacemark?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var routeButton: UIButton!
    
    @IBAction func routeButton(_ sender: UIButton) {
        // Se está a decorrer um percurso
        if  tracking {
            tracking = false
            routeButton.setTitle("Go", for: .normal)
            routeButton.backgroundColor = UIColor.green
            
            // Calcular distância total do percurso
            var totalDistance = 0.0
            if route.count > 1 {
                for i in 1...(route.count-1) {
                    totalDistance += route[i].distance(from: route[i-1])
                }
                let eTime = NSDate().timeIntervalSince1970
                dateform.setLocalizedDateFormatFromTemplate("dd-MM-YYYY HH-mm-ss")
                let initTime = dateform.string(from: Date(timeIntervalSince1970: iTime))
                let endTime = dateform.string(from: Date(timeIntervalSince1970: eTime))
                let duration = eTime - iTime
                let initPlace = ""
                let endPlace = ""
                let velocity = (totalDistance/duration)*3.6
                
                let currentRoute = Route(route: route, initTime: initTime, endTime: endTime, distance: totalDistance, duration: duration, initPlace: initPlace, endPlace: endPlace, velocity: velocity, eTime: eTime)
                saveRouteAlert(message: "Guardar percurso? Fez \(convertDistance(distance: totalDistance))", saveTitle: "Guardar", cancelTitle: "Cancelar", vc: self, currentRoute: currentRoute)
                
                // Limpar o mapa
                mapView.removeOverlays(mapView.overlays)
            }
            route = [CLLocation]()
            
        // Se ainda não há nenhum percurso ativo
        } else {
            tracking = true
            routeButton.setTitle("Stop", for: .normal)
            routeButton.backgroundColor = UIColor.red
            AppDelegate.route.append(currentLocation)
            iTime = NSDate().timeIntervalSince1970
        }
    }
    
    // Permite fazer zoom para a posição atual
    @IBAction func zoomInButton(_ sender: UIButton) {
        mapView.region = MKCoordinateRegionMake(currentLocation.coordinate, MKCoordinateSpanMake(0.005, 0.005))
    }
    
    // Colocar pin no local carrgado no mapa
    @IBAction func tappedScreen(_ sender: UITapGestureRecognizer) {
        let tapPoint = sender.location(in: mapView)
        let tappedLocation = mapView.convert(tapPoint, toCoordinateFrom: mapView)
        let tappedPlacemark = MKPlacemark(coordinate: tappedLocation)
        mapView.removeOverlays(mapView.overlays)
        dropPinZoomIn(placemark: tappedPlacemark)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configuração do locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        
        // Instancia a vista com a tabela de pesquisa e faz as configurações necessárias
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTableViewController") as! LocationSearchTableViewController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Navegar até..."
        navigationItem.titleView = resultSearchController?.searchBar
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // First responder para shake gesture
        becomeFirstResponder()
        
        mapView.removeOverlays(mapView.overlays)
        mapView.region = MKCoordinateRegionMake(currentLocation.coordinate, MKCoordinateSpanMake(0.005, 0.005))
        
        // Desenha o percurso atual quando volta para a vista do mapa
        if route.count > 1{
            for i in 1...route.count-1{
                var area = [route[i].coordinate, route[i-1].coordinate]
                let polyline = MKPolyline(coordinates: &area, count: area.count)
                blueRoute = true
                mapView.add(polyline)
            }
        }
        if tracking {
            routeButton.setTitle("Stop", for: .normal)
            routeButton.backgroundColor = UIColor.red
        }
        // Caso receba um posto de carregamento desenha as direções
        if let s = station {
            selectedPin = MKPlacemark(coordinate: s.location.coordinate)
            drawDirections()
            AppDelegate.doSegueToMap = false
        }
    }
    
    // Desenha as direções para o local desejado
    func drawDirections (){
        guard let currentPlacemark = selectedPin else {
            return
        }
        
        let directionRequest = MKDirectionsRequest()
        let destinationPLacemark = MKPlacemark(placemark: currentPlacemark)
        directionRequest.source = MKMapItem.forCurrentLocation()
        directionRequest.destination = MKMapItem(placemark: destinationPLacemark)
        directionRequest.transportType = .any
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (directionsResponse, error) in
            guard let directionsResponse = directionsResponse else {
                if let error = error {
                    print("Error getting directions: \(error.localizedDescription)")
                }
                return
            }
            let suggestedRoute = directionsResponse.routes[0]
            self.blueRoute = false
            self.mapView.removeOverlays(self.mapView.overlays)
            self.mapView.add(suggestedRoute.polyline, level: .aboveRoads)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    //Quando atualiza a localização desenha o percurso atual e desenha as direções caso seja pretendido
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        if let c = locationObj {
            mapView.removeOverlays(mapView.overlays)
            currentLocation = c
            if tracking{
                route.append(c)
                let numPoints = route.count
                if numPoints > 1{
                    for i in 1...numPoints-1{
                        var area = [route[i].coordinate, route[i-1].coordinate]
                        var polyline = MKPolyline(coordinates: &area, count: area.count)
                        blueRoute = true
                        mapView.add(polyline)
                    }
                }
            }
            drawDirections()
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("location manager error:: \(error)")
        }
    }
    
    // colocar pin no mapa e fazer zoom
    func dropPinZoomIn(placemark:MKPlacemark){
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality {
            annotation.subtitle = "\(city)"
        }
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.02, 0.02)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
        drawDirections()
    }
    
    // Desenhar o pin
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        pinView?.pinTintColor = UIColor.red
        pinView?.canShowCallout = true
        let button = UIButton(type: .detailDisclosure) as UIButton
        pinView?.leftCalloutAccessoryView = button
        
        return pinView
    }
    
    // Desenha os overlays em cima do mapa (azul para o percurso atual e laranja para as direções para o local pretendido)
    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer!{
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            if blueRoute{
                pr.strokeColor = UIColor.blue
                pr.lineWidth = 3
            } else {
                pr.strokeColor = UIColor.orange
                pr.lineWidth = 5
            }
            return pr
        }
        return nil
    }
    
    // Quando se termina um percurso aparece um alert que permite guardar ou não o mesmo
    func saveRouteAlert(message: String, saveTitle: String, cancelTitle: String, vc: UIViewController, currentRoute: Route)
    {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        // Guardar routes quando se carrega em OK
        let OKAction = UIAlertAction(title: saveTitle, style: .default, handler: { action in
            eBikeLifeRepository.repository.routes.append(currentRoute)
            eBikeLifeRepository.repository.saveRoutes()
            self.coordToRegion(coordinates: currentRoute.route[0], startPlace: true)
            self.coordToRegion(coordinates: currentRoute.route.last!, startPlace:false)
        })
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    // geocode e coordToRegion permitem fazer o reverse geocoding de forma a obter o nome do local através das coordenadas
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemarks, error in
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }
    
    func coordToRegion(coordinates : CLLocation, startPlace : Bool){
        let latitude  : Double = coordinates.coordinate.latitude
        let longitude : Double = coordinates.coordinate.longitude
        var region : String = ""
        geocode(latitude: latitude, longitude: longitude) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            // you should always update your UI in the main thread
            DispatchQueue.main.async {
                if placemark.thoroughfare != nil{
                    region = placemark.thoroughfare ?? ""
                    region.append(", \(placemark.locality ?? "")")
                } else {
                    if placemark.subLocality != nil {
                        region = placemark.subLocality ?? ""
                        region.append(", \(placemark.locality ?? "")")
                    } else {
                        region = placemark.locality ?? ""
                    }
                }
                
                // grava diretamente o local de início e fim
                if startPlace {
                    eBikeLifeRepository.repository.routes.last?.initPlace = region
                    eBikeLifeRepository.repository.saveRoutes()
                } else {
                    eBikeLifeRepository.repository.routes.last?.endPlace = region
                    eBikeLifeRepository.repository.saveRoutes()
                }
            }
        }
    }
    
    // First responder para shake gesture
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    // Voltar para menu inicial quando se faz shake
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            if let nav = navigationController {
                nav.popToRootViewController(animated: true)
            }
        }
    }
    
    // Permite converter metros em kilometros se necessário
    func convertDistance(distance: Double) -> String{
        if distance < 1000 {
            return String(format: "%.0fm", distance)
        } else {
            return String(format: "%.2fkm", distance/1000)
        }
    }
}
