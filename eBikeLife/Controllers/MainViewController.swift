//
//  MainViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit
import CoreLocation

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate {
    
    var forecastData = [Weather]()
    var icon = Array(repeating: "", count:40)
    var temp = Array(repeating: "", count:40)
    var hour = Array(repeating: "", count:40)
    var day = Array(repeating: "", count:40)
    var lastWeatherUpdateLocation = CLLocation()
    var doSegueToMap = false
    var isCheckingSegue = false
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    // Atualiza a variável local quando a variável do AppDelegate é alterada
    var locationManager: CLLocationManager {
        return AppDelegate.locationManager
    }
    var currentLocation:CLLocation{
        get{
            return AppDelegate.currentLocation
        }
        set (newL){
            AppDelegate.currentLocation = newL
        }
    }
    
    var tracking: Bool {
        get{
            return AppDelegate.tracking
        }
        set (newTracking){
            AppDelegate.tracking = newTracking
        }
    }
    
    @IBOutlet weak var distanceBar: CircleProgressBar!
    @IBOutlet weak var timeBar: CircleProgressBar!
    @IBOutlet weak var savingsBar: CircleProgressBar!
    @IBOutlet weak var meteorology: UICollectionView!
    @IBOutlet weak var chargingButton: UIButton!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var selector: UISegmentedControl!
    @IBOutlet weak var startRouteButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var chargingImage: UIImageView!
    
    // seletor da visualização dos dados das barras de progresso circulares
    @IBAction func selectorSpanChanged(_ sender: UISegmentedControl) {
        var totalDistance = 0.0
        var totalTime = 0.0
        let count = eBikeLifeRepository.repository.routes.count
        let calendar = Calendar(identifier: .gregorian)
        let now = NSDate().timeIntervalSince1970
        let date = Date(timeIntervalSince1970: now)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let week = calendar.component(.weekOfYear, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        let dayOfWeek = calendar.component(.weekday, from: date)
        let day = calendar.component(.day, from: date)
        
        // calcula os valores para as progressbars consoante o segmento selecionado
        switch sender.selectedSegmentIndex {
        case 0: // searchToday
            if count > 0 {
                for i in 0...count-1 {
                    let date = Date(timeIntervalSince1970: eBikeLifeRepository.repository.routes[i].eTime)
                    if calendar.isDateInToday(date){
                        totalDistance += eBikeLifeRepository.repository.routes[i].distance
                        totalTime += eBikeLifeRepository.repository.routes[i].duration
                    }
                }
            }
            if (totalTime > Double(hour*3600 + minute*60 + second)){
                totalTime = Double(hour*3600 + minute*60 + second)
            }
            let save = (1.6*(CGFloat(totalDistance)*6/100000));
            refreshProgressBars(goalDist: CGFloat(eBikeLifeRepository.repository.objectives[0].dayDist), goalTime: CGFloat(eBikeLifeRepository.repository.objectives[0].dayTime), goalSav: CGFloat(eBikeLifeRepository.repository.objectives[0].daySave), Dist: CGFloat(totalDistance), Time: CGFloat(totalTime), Savings: save)
        case 1:// searchWeek
            if count > 0 {
                for i in 0...count-1 {
                    let routeDate = Date(timeIntervalSince1970: eBikeLifeRepository.repository.routes[i].eTime)
                    let routeWeek = calendar.component(.weekOfYear, from: routeDate)
                    if routeWeek == week{
                        totalDistance += eBikeLifeRepository.repository.routes[i].distance
                        totalTime += eBikeLifeRepository.repository.routes[i].duration
                    }
                }
            }
            if (totalTime > Double(dayOfWeek*24*3600 + hour*3600 + minute*60 + second)){
                totalTime = Double(dayOfWeek*24*3600 + hour*3600 + minute*60 + second)
            }
            let save = (1.6*(CGFloat(totalDistance)*6/100000));
            refreshProgressBars(goalDist: CGFloat(eBikeLifeRepository.repository.objectives[0].weekDist), goalTime: CGFloat(eBikeLifeRepository.repository.objectives[0].weekTime), goalSav: CGFloat(eBikeLifeRepository.repository.objectives[0].weekSave), Dist: CGFloat(totalDistance), Time: CGFloat(totalTime), Savings: save)
        case 2:// searchMonth
            if count > 0 {
                for i in 0...count-1 {
                    let routeDate = Date(timeIntervalSince1970: eBikeLifeRepository.repository.routes[i].eTime)
                    let routeMonth = calendar.component(.month, from: routeDate)
                    let routeYear = calendar.component(.year, from: routeDate)
                    if  routeMonth == month && routeYear == year{
                        totalDistance += eBikeLifeRepository.repository.routes[i].distance
                        totalTime += eBikeLifeRepository.repository.routes[i].duration
                    }
                }
            }
            
            if (totalTime > Double(day*24*3600 + hour*3600 + minute*60 + second)){
                totalTime = Double(day*24*3600 + hour*3600 + minute*60 + second)
            }
            let save = (1.6*(CGFloat(totalDistance)*6/100000));
            refreshProgressBars(goalDist: CGFloat(eBikeLifeRepository.repository.objectives[0].monthDist), goalTime: CGFloat(eBikeLifeRepository.repository.objectives[0].monthTime), goalSav: CGFloat(eBikeLifeRepository.repository.objectives[0].monthSave), Dist: CGFloat(totalDistance), Time: CGFloat(totalTime), Savings: save)
        case 3:// searchAlways
            if count > 0 {
                for i in 0...count-1 {
                    totalDistance += eBikeLifeRepository.repository.routes[i].distance
                    totalTime += eBikeLifeRepository.repository.routes[i].duration
                }
            }
            let save = (1.6*(CGFloat(totalDistance)*6/100000));
            refreshProgressBars(goalDist: CGFloat(totalDistance), goalTime: CGFloat(totalTime), goalSav: save, Dist: CGFloat(totalDistance), Time: CGFloat(totalTime), Savings: save)
        default:
            print("Unexpected Error!! Dun kno wut do :(")
        }
    }
    
    // Permite começaar ou terminar um carregamento quando se carrega no botão
    @IBAction func startCharging(_ sender: UIButton) {
        let dateform = DateFormatter()
        dateform.setLocalizedDateFormatFromTemplate("dd-MM-YYYY HH-mm-ss")
        if eBikeLifeRepository.repository.charges.isEmpty || (eBikeLifeRepository.repository.charges.last?.finished)! {
            
            // verifica se está perto de um posto de carregamento
            if checkChargingLocation() {
                chargingButton.setTitle("Parar Carregamento", for: .normal)
                chargingImage.isHidden = false
                let iTime = NSDate().timeIntervalSince1970
                let initTime = dateform.string(from: Date(timeIntervalSince1970: iTime))
                let place = decidePlace()
                let currentCharge = Charge(place: place, initTime: initTime, endTime: "", duration: 0.0, finished: false, iTime: iTime)
                eBikeLifeRepository.repository.charges.append(currentCharge)
                eBikeLifeRepository.repository.saveCharges()
            } else {
                displayChargingAlert(message: "Não se encontra nas imediações de nenhum posto de carregamento", okTitle: "Ok", vc: self)
            }
        } else {
            chargingButton.setTitle("Iniciar Carregamento", for: .normal)
            chargingImage.isHidden = true
            let eTime = NSDate().timeIntervalSince1970
            let endTime = dateform.string(from: Date(timeIntervalSince1970: eTime))
            let duration = eTime - (eBikeLifeRepository.repository.charges.last?.iTime)!
            eBikeLifeRepository.repository.charges.last?.duration = duration
            eBikeLifeRepository.repository.charges.last?.endTime = endTime
            eBikeLifeRepository.repository.charges.last?.finished = true
            let stringToPresent = convertTime(duration: duration)
            eBikeLifeRepository.repository.saveCharges()
            saveAlert(message: "Guardar Carregamento? \nDuração de carregamento: \(stringToPresent)", saveTitle: "Guardar", cancelTitle: "Não Guardar", vc: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configurar location manager e weather
        meteorology.delegate = self
        meteorology.dataSource = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        // Localização dos postos de carregamento são sempre iguais pelo que são adicionados "manualmente"
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.734704 ,longitude: -8.821045 ), place : "ESTG"))
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.732761 ,longitude: -8.820380 ), place : "ESSLEI"))
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.365109 ,longitude: -9.403230 ), place : "ESTM"))
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.740154 ,longitude: -8.811364 ), place : "ESECS"))
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.395413 ,longitude: -9.135504 ), place : "ESAD"))
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.723111 ,longitude: -8.930229 ), place : "CDRsp"))
        eBikeLifeRepository.repository.stations.append(Station(location : CLLocation(latitude: 39.737346 ,longitude: -8.810675 ), place : "Serviços Centrais"))
        
        if eBikeLifeRepository.repository.charges.isEmpty || (eBikeLifeRepository.repository.charges.last?.finished)! {
            chargingButton.setTitle("Iniciar Carregamento", for: .normal)
            
        } else {
            chargingButton.setTitle("Parar Carregamento", for: .normal)
            chargingImage.isHidden = false
        }
        
        // Caso não existam objetivos definidos (primeira vez que se utiliza a aplicação) estes são adicionados
        if eBikeLifeRepository.repository.objectives.count == 0 {
            let newObjectives = Objectives(dayDist: 10000, dayTime: 3600, daySave: 5, weekDist: 100000, weekTime: 36000, weekSave: 50, monthDist: 300000, monthTime: 30*3600, monthSave: 150)
            eBikeLifeRepository.repository.objectives.append(newObjectives)
            eBikeLifeRepository.repository.saveObjectives()
        }
        
        sideMenus()
        
        updateWeather(location: currentLocation)
        lastWeatherUpdateLocation = currentLocation
        
        // Inicializar a progressBar com valores
        var totalDistance = 0.0
        var totalTime = 0.0
        let count = eBikeLifeRepository.repository.routes.count
        let calendar = Calendar(identifier: .gregorian)
        if count > 0 {
            for i in 0...count-1 {
                let date = Date(timeIntervalSince1970: eBikeLifeRepository.repository.routes[i].eTime)
                if calendar.isDateInToday(date){
                    totalDistance += eBikeLifeRepository.repository.routes[i].distance
                    totalTime += eBikeLifeRepository.repository.routes[i].duration
                }
            }
        }
        refreshProgressBars(goalDist: 100, goalTime: 180, goalSav: 20, Dist: CGFloat(totalDistance), Time: CGFloat(totalTime), Savings: 2)
        chargingButton.titleLabel?.adjustsFontSizeToFitWidth = true
        startRouteButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    // Quando a vista aparece, seleciona sempre o mesmo semento do selector
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        selector.selectedSegmentIndex = 0
        selector.sendActions(for: UIControlEvents.valueChanged)
        if tracking {
            startRouteButton.setTitle("Ir para percurso", for: .normal)
        } else {
            startRouteButton.setTitle("Iniciar percurso", for: .normal)
        }
        
        // Verifica se tem de fazer o segue para a vista do mapa (por causa do SWRevealViewController)
        if !isCheckingSegue {
            isCheckingSegue = true
            checkSegueToMap()
        }
    }
    
    // apresenta sempre meteorologia para 40 horas
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 40
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    // Faz update da meteorologia a cada 10km
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        if let c = locationObj {
            currentLocation = c
            
            if lastWeatherUpdateLocation.distance(from: c) > 10000 {
                updateWeather(location: c)
                lastWeatherUpdateLocation = c
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("location manager error:: \(error)")
        }
    }
    
    // Criar os itens da collection view do Weather
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = meteorology.dequeueReusableCell(withReuseIdentifier: "customCell", for: indexPath) as! MeteorologyViewCellCollectionViewCell
        cell.hourLabel.text = hour[indexPath.row]
        cell.weekDayLabel.text = day[indexPath.row]
        cell.temperatureLabel.text = temp[indexPath.row]
        cell.weatherImage.image = UIImage(named: icon[indexPath.row])
        return cell
    }
    
    func updateWeather (location: CLLocation){
        coordToRegion(coordinates: location)
        
        // pedido com localização atual
        Weather.forecast(withLocation: location.coordinate, completion: { (results:[Weather]?) in
            
            if let weatherData = results {
                if !weatherData.isEmpty {
                    self.forecastData = weatherData
                    DispatchQueue.main.async {
                        let dateform = DateFormatter()
                        dateform.setLocalizedDateFormatFromTemplate("HH-mm")
                        
                        for i in 0...39{
                            self.icon[i] = weatherData[i].icon
                            self.day[i] = self.getWeekDay(since70: weatherData[i].time)
                            self.temp[i] = "\(Int((weatherData[i].temperature).rounded()))º"
                            self.hour[i] = dateform.string(from: Date(timeIntervalSince1970: weatherData[i].time))
                        }
                        self.meteorology.reloadData()
                    }
                }
            }
        })
    }
    
    // Verifica qual o posto de carregamento mais próximo
    func decidePlace() -> String {
        var minDist = 9999.0
        var place = ""
        for i in 0...(eBikeLifeRepository.repository.stations.count-1) {
            let distance = eBikeLifeRepository.repository.stations[i].location.distance(from: currentLocation)
            if distance < minDist {
                minDist = distance
                place = eBikeLifeRepository.repository.stations[i].place
            }
        }
        return place
    }
    
    // Permite transformar um valor em segundos para horas:minutos:segundos
    func convertTime(duration: Double) -> String{
        if duration < 60 {
            let seconds = round(duration)
            return String(format: "%.0fs", seconds)
        } else if (duration > 60 && duration < 3600) {
            let minutes = floor(duration/60)
            let seconds = round (duration - minutes*60)
            return String(format: "%.0fm %.0fs",minutes, seconds)
        } else if (duration > 3600 && duration < 86400) {
            let hours = floor(duration/3600)
            let minutes = floor((duration - hours*3600)/60)
            let seconds = round(duration - hours*3600 - minutes*60)
            return String(format: "%.0fh %.0fm %.0fs", hours, minutes, seconds)
        } else {
            let days = floor(duration/86400)
            let hours = floor((duration - days*86400)/3600)
            let minutes = floor((duration - days*86400 - hours*3600)/60)
            return String(format: "%.0fd %.0fh %.0fm",days , hours, minutes)
        }
    }
    
    // Alert que permite ao utilizador guardar ou não um determinado carregamento
    func saveAlert(message: String, saveTitle: String, cancelTitle: String, vc: UIViewController) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: saveTitle, style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { action in
            eBikeLifeRepository.repository.charges.removeLast()
            eBikeLifeRepository.repository.saveCharges()
        })
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    // Display alert quando não se encontra perto de um posto de carregamento
    func displayChargingAlert(message: String, okTitle: String, vc: UIViewController) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: okTitle, style: .default, handler:  nil)
        alertController.addAction(OKAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    // Atualiza as barras de progresso circulares
    func refreshProgressBars(goalDist: CGFloat, goalTime: CGFloat, goalSav: CGFloat, Dist: CGFloat, Time: CGFloat, Savings: CGFloat){
        var progress: CGFloat = Dist/goalDist;
        distanceBar.setProgress(progress, animated: true)
        distanceBar.hintTextFont = UIFont(name: "HelveticaNeue-CondensedBlack", size: screenWidth/12)
        distanceBar.hintViewSpacing = 15
        distanceBar.setHintTextGenerationBlock { (progress) -> String? in
            return self.convertDistance(distance: Double(Dist))
        }
        
        progress = Time/goalTime;
        timeBar.setProgress(progress, animated: true)
        timeBar.hintTextFont = UIFont(name: "HelveticaNeue-CondensedBlack", size: screenWidth/15)
        timeBar.hintViewSpacing = 12
        timeBar.setHintTextGenerationBlock { (progress) -> String? in
            return self.convertTime(duration: Double(Time))
        }
        
        progress = Savings/goalSav;
        savingsBar.setProgress(progress, animated: true)
        savingsBar.hintTextFont = UIFont(name: "HelveticaNeue-CondensedBlack", size: screenWidth/20)
        savingsBar.hintViewSpacing = 10
        savingsBar.setHintTextGenerationBlock { (progress) -> String? in
            return String.init(format: "%.2f€", arguments: [Savings])
        }
        
        
        
    }
    
    // Obtenção do dia da semana para a meteorologia
    func getWeekDay(since70: Double) -> String {
        let date = Date(timeIntervalSince1970: since70)
        let calendar = Calendar(identifier: .gregorian)
        let weekday = calendar.component(.weekday, from: date)
        
        switch weekday {
        case 1:
            return "Dom"
        case 2:
            return "Seg"
        case 3:
            return "Ter"
        case 4:
            return "Qua"
        case 5:
            return "Qui"
        case 6:
            return "Sex"
        case 7:
            return "Sab"
        default:
            return "Err"
        }
    }
    
    //Mostra menu lateral
    func sideMenus() {
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rearViewRevealWidth = screenWidth*4/5
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    // Verifica se está nas imediações de um posto de carregamento
    func checkChargingLocation() -> Bool{
        for i in 0...(eBikeLifeRepository.repository.stations.count-1) {
            let distance = eBikeLifeRepository.repository.stations[i].location.distance(from: currentLocation)
            if distance < 500 {
                return true
            }
        }
        return false
    }
    
    // Verifica se é necessário ir para a vista do mapa (isto acontece quando se pretende ir até um posto de carregamento)
    func checkSegueToMap(){
        DispatchQueue.global(qos: .background).async {
            while !self.doSegueToMap {
                if AppDelegate.doSegueToMap {
                    self.doSegueToMap = true
                }
            }
            
            DispatchQueue.main.async {
                self.doSegueToMap = false
                self.isCheckingSegue = false
                self.performSegue(withIdentifier: "showChargingStationSegue", sender: AppDelegate.chargingStation)
                
            }
        }
    }
    
    // Envia o posto de carregamento pretendido para a vista do mapa
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showChargingStationSegue" {
            if let controller = segue.destination as? MapViewController {
                controller.station = sender as? Station
            }
        }
    }
    
    // Faz reverse geocoding de forma a obter local em função das coordenadas
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemarks, error in
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }
    
    func coordToRegion(coordinates : CLLocation){
        let latitude  : Double = coordinates.coordinate.latitude
        let longitude : Double = coordinates.coordinate.longitude
        var region : String = ""
        geocode(latitude: latitude, longitude: longitude) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            DispatchQueue.main.async {
                if placemark.subLocality != nil {
                    region = placemark.subLocality ?? ""
                    region.append(", \(placemark.locality ?? "")")
                    region.append(", \(placemark.administrativeArea ?? "")")
                } else {
                    region = placemark.locality ?? ""
                    region.append(", \(placemark.administrativeArea ?? "")")
                }
                self.locationLabel.text = region
            }
        }
    }
    
    // Permite converter metros em kilometros se necessário
    func convertDistance(distance: Double) -> String{
        if distance < 1000 {
            return String(format: "%.0fm", distance)
        } else {
            return String(format: "%.2fkm", distance/1000)
        }
    }
}
