//
//  RouteHistViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit
import CoreLocation

class RouteHistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var localList = [Route]()
    var doingSegue = false
    var route = Route(route: [CLLocation(latitude: 0,longitude: 0)], initTime: "0", endTime: "0", distance: 0, duration: 0, initPlace: "0", endPlace: "0", velocity: 0, eTime: 0)
    @IBOutlet weak var selector: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    //Permite organizar os percursos dependendo do segmento
    @IBAction func segmentedControlActionChange(_ sender: UISegmentedControl) {
        switch selector.selectedSegmentIndex {
        case 0:
            localList.sort(by: {$0.eTime > $1.eTime})
            break
        case 1:
            localList.sort(by: {$0.distance > $1.distance})
            break
        case 2:
            localList.sort(by: {$0.duration > $1.duration})
            break
        default:
            break
        }
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        localList = eBikeLifeRepository.repository.routes
        selector.selectedSegmentIndex = 0
        segmentedControlActionChange(selector)
        
        // Ajustar a largura da view do side menu
        let newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*4/5, height: UIScreen.main.bounds.height)
        self.view.frame = newFrame
        doingSegue = false
    }
    
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Se não for fazer o segue, volta para trás
        if !doingSegue {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        }
    }
    
    // Devolve número de percursos
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localList.count
    }
    
    // Atualiza a lista com as informações dos percursos
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        myCell.textLabel?.numberOfLines = 5
        myCell.textLabel!.text = ("De: \(localList[indexPath.row].initPlace) \nAté: \(localList[indexPath.row].endPlace) \nTerminado em: \(localList[indexPath.row].endTime) \nDistância: \(convertDistance(distance: localList[indexPath.row].distance)) \nDuração: \(convertTime(duration: localList[indexPath.row].duration))")
        myCell.textLabel?.adjustsFontSizeToFitWidth = true
        return myCell
    }
    
    // Quando se seleciona um percurso faz segue para a vista de detalhes e envia o percurso selecionado
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        route = localList[indexPath.row]
        doingSegue = true
        self.performSegue(withIdentifier: "detalhesPercursoSegue", sender: route)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    // Função que apaga o percurso da lista e atualiza a tabela
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            for index in 0...eBikeLifeRepository.repository.charges.count-1{
                if eBikeLifeRepository.repository.routes[index] == localList[indexPath.row]{
                    eBikeLifeRepository.repository.routes.remove(at: index)
                    break
                }
            }
            localList.remove(at: indexPath.row)
            eBikeLifeRepository.repository.saveRoutes()
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "detalhesPercursoSegue" {
            if let controller = segue.destination as?  RouteDetailedViewController{
                controller.route = route
            }
        }
    }
    
    // Permite transformar um valor em segundos para horas:minutos:segundos
    func convertTime(duration: Double) -> String{
        if duration < 60 {
            let seconds = round(duration)
            return String(format: "%.0fs", seconds)
        } else if (duration > 60 && duration < 3600) {
            let minutes = floor(duration/60)
            let seconds = round (duration - minutes*60)
            return String(format: "%.0fm %.0fs",minutes, seconds)
        } else if (duration > 3600 && duration < 86400) {
            let hours = floor(duration/3600)
            let minutes = floor((duration - hours*3600)/60)
            let seconds = round(duration - hours*3600 - minutes*60)
            return String(format: "%.0fh %.0fm %.0fs", hours, minutes, seconds)
        } else {
            let days = floor(duration/86400)
            let hours = floor((duration - days*86400)/3600)
            let minutes = floor((duration - days*86400 - hours*3600)/60)
            return String(format: "%.0fd %.0fh %.0fm",days , hours, minutes)
        }
    }
    
    // Permite converter metros em kilometros se necessário
    func convertDistance(distance: Double) -> String{
        if distance < 1000 {
            return String(format: "%.0fm", distance)
        } else {
            return String(format: "%.2fkm", distance/1000)
        }
    }
}
