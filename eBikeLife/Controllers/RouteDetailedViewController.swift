//
//  RouteDetailedViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit
import MapKit

class RouteDetailedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate{
    
    var route:Route?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Ajustar a largura da view do side menu
        let newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*4/5, height: UIScreen.main.bounds.height)
        self.view.frame = newFrame
        
        mapView.region = MKCoordinateRegionMake((route?.route[0].coordinate)!, MKCoordinateSpanMake(0.005, 0.005))
        mapView.delegate = self
        // Desenha o percurso no mapa
        for i in 1...(route?.route.count)!-1{
            var area = [(route?.route[i].coordinate)!, (route?.route[i-1].coordinate)!]
            let polyline = MKPolyline(coordinates: &area, count: area.count)
            mapView.add(polyline)
        }
    }

    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        let text = String(format : "Fiz %.2fkm em %@ com uma velocidade média de %.1f km/h.\nComeçei em %@ ás %@ e terminei em %@ ás %@.",
                          (route?.distance)!/1000,
                          convertTime(duration: (route?.duration)!),
                          (route?.velocity)!,
                          (route?.initPlace)!,
                          (route?.initTime)!,
                          (route?.endPlace)!,
                          (route?.endTime)!)
        
        let activityViewController = UIActivityViewController(activityItems: [ text ], applicationActivities: nil)
        // para ipad:
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    // Mostra na tabela as informações do percurso
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "myCellDetailed", for: indexPath)
        myCell.textLabel!.text = route?.initTime
        myCell.detailTextLabel!.text = route?.endPlace
        
        switch indexPath.row {
            case 0: //duracao
                myCell.textLabel!.text = "Duração: "
                myCell.detailTextLabel!.text = convertTime(duration: (route?.duration)!)
            case 1: //Distancia
                myCell.textLabel!.text = "Distância: "
                myCell.detailTextLabel!.text = convertDistance(distance: (route?.distance)!)
            case 2: //velocidade media
                myCell.textLabel!.text = "Velocidade média: "
                myCell.detailTextLabel!.text = String(format : "%.2f km/h", (route?.velocity)!)
            case 3: //init time
                myCell.textLabel!.text = "Início:"
                myCell.detailTextLabel!.text = route?.initTime
            case 4: //end time
                myCell.textLabel!.text = "Fim:"
                myCell.detailTextLabel!.text = route?.endTime
            case 5: //init place
                myCell.textLabel!.text = "Local de início:"
                myCell.detailTextLabel!.text = route?.initPlace
            case 6: //end place
                myCell.textLabel!.text = "Local de fim:"
                myCell.detailTextLabel!.text = route?.endPlace
            default:
                print("nothing")
        }
        myCell.textLabel?.adjustsFontSizeToFitWidth = true
        myCell.detailTextLabel?.adjustsFontSizeToFitWidth = true
        return myCell
    }
    
    // Permite transformar um valor em segundos para horas:minutos:segundos
    func convertTime(duration: Double) -> String{
        if duration < 60 {
            let seconds = round(duration)
            return String(format: "%.0fs", seconds)
        } else if (duration > 60 && duration < 3600) {
            let minutes = floor(duration/60)
            let seconds = round (duration - minutes*60)
            return String(format: "%.0fm %.0fs",minutes, seconds)
        } else if (duration > 3600 && duration < 86400) {
            let hours = floor(duration/3600)
            let minutes = floor((duration - hours*3600)/60)
            let seconds = round(duration - hours*3600 - minutes*60)
            return String(format: "%.0fh %.0fm %.0fs", hours, minutes, seconds)
        } else {
            let days = floor(duration/86400)
            let hours = floor((duration - days*86400)/3600)
            let minutes = floor((duration - days*86400 - hours*3600)/60)
            return String(format: "%.0fd %.0fh %.0fm",days , hours, minutes)
        }
    }
    
    // Permite converter metros em kilometros se necessário
    func convertDistance(distance: Double) -> String{
        if distance < 1000 {
            return String(format: "%.0fm", distance)
        } else {
            return String(format: "%.2fkm", distance/1000)
        }
    }
    
    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer!{
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = UIColor.blue
            pr.lineWidth = 3
            return pr
        }
        return nil
    }
}
