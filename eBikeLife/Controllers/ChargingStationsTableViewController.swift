//
//  ChargingStationsTableViewController.swift
//  eBikeLife
//
//  Created by André Sardo, Mário Vala  and Ihor Koval on May 2018.
//  Copyright © 2018 IPLeiria. All rights reserved.
//

import UIKit
import CoreLocation

class ChargingStationsTableViewController: UITableViewController {

    // localização atualizada
    var localList = [Station]()
    
    // Atualiza a variável local quando a variável do AppDelegate é alterada
    var currentLocation:CLLocation{
        get{
            return AppDelegate.currentLocation
        }
        set (newL){
            AppDelegate.currentLocation = newL
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localList = eBikeLifeRepository.repository.stations
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Ajustar a largura da view do side menu
        let newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*4/5, height: UIScreen.main.bounds.height)
        self.view.frame = newFrame
        
        // Organiza por posto de carregamento mais perto
        localList.sort(by: {$0.location.distance(from: currentLocation) < $1.location.distance(from: currentLocation)})
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Se não for fazer o segue, volta para trás
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eBikeLifeRepository.repository.stations.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let distance = localList[indexPath.row].location.distance(from: currentLocation)
        let myCell = tableView.dequeueReusableCell(withIdentifier: "myCell3", for: indexPath)
        myCell.textLabel!.text = ("\(localList[indexPath.row].place) a \(convertDistance(distance: distance))")
        myCell.textLabel?.adjustsFontSizeToFitWidth = true
        return myCell
    }
    
    //Quando se seleciona um posto de carregamento é pretendido que vá para o mapa e desenhe as direções para o posto selecionado
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chargingStation = localList[indexPath.row]
        AppDelegate.chargingStation = chargingStation
        AppDelegate.doSegueToMap = true
        
        // Esconde o side menu
        revealViewController().revealToggle(animated: true)
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        dismiss(animated: true, completion: nil)
    }
    
    // Permite converter metros em kilometros se necessário
    func convertDistance(distance: Double) -> String{
        if distance < 1000 {
            return String(format: "%.0fm", distance)
        } else {
            return String(format: "%.2fkm", distance/1000)
        }
    }
}
